package proj2;

import java.util.ArrayList;
import java.util.List;

import proj2.proj2.Position;

public class proj2 {
	public static Tree<String> tree;
	public static List<String> preOrderMasterList;
	public static List<String> postOrderMasterList;
	public static List<Integer> indexes = new ArrayList<Integer>();
	
	public static void main(String[] args) {
		
		// THIS SET WORKS
//		String preOrder = "< H, B, G, M, W, F, T, X, Z, C, R, P";
//		String postOrder = "> G, M, W, F, B, X, Z, T, R, P, C, H";
		
		
		// THIS SET DOES NOT WORK!!!! 4 levels breaks for some reason
//		preOrder = "< D, H, B, G, M, W, F, T, X, Z, C, R, P, Q, N";
//		postOrder = "> G, M, W, F, B, X, Z, T, R, P, C, H, N, Q, D"; 
		
		// THIS SET WILL WORK
//		preOrder = "< c, r, p";
//		postOrder = "> r, p, c";
		
		
		preOrderMasterList = new ArrayList<String>();
		preOrderMasterList.add("H");
		preOrderMasterList.add("B");
		preOrderMasterList.add("G");
		preOrderMasterList.add("M");
		preOrderMasterList.add("W");
		preOrderMasterList.add("F");
		preOrderMasterList.add("T");
		preOrderMasterList.add("X");
		preOrderMasterList.add("Z");
		preOrderMasterList.add("C");
		preOrderMasterList.add("R");
		preOrderMasterList.add("P");
		
		postOrderMasterList = new ArrayList<String>();
		postOrderMasterList.add("G");
		postOrderMasterList.add("M");
		postOrderMasterList.add("W");
		postOrderMasterList.add("F");
		postOrderMasterList.add("B");
		postOrderMasterList.add("X");
		postOrderMasterList.add("Z");
		postOrderMasterList.add("T");
		postOrderMasterList.add("R");
		postOrderMasterList.add("P");
		postOrderMasterList.add("C");
		postOrderMasterList.add("H");

		tree = new Tree<String>();
		
		tree.setRoot(buildTree(postOrderMasterList.size(), 0, postOrderMasterList.size() - 1));
		
		
		// Display tree output
		List<Position<String>> rootList = new ArrayList<Position<String>>();
		rootList.add(tree.root());
		System.out.println("Nodes by level:");
		tree.levelOrderTraversal(rootList);
		System.out.println("Nodes listed with their parent:");
		tree.printTree(tree.root());
	}
	
	public static Position<String> buildTree(int size, int prestart, int poststart) {
		Position<String> parentNode = new Position<String>(preOrderMasterList.get(prestart), null);
		prestart++;
		for (int i = prestart; i <= poststart; i++) {
			int locatedAt = postOrderMasterList.indexOf(preOrderMasterList.get(i));
			if (prestart < locatedAt) {
				Position<String> subTree = buildTree(0, prestart, locatedAt + 1);
				subTree.setParent(parentNode);
				parentNode.addChild(subTree);
			} else {
				// Verify we haven't added it already somewhere
				if (indexes.indexOf(locatedAt) == -1) {
					// If this is a leaf, tack it on as a child
					parentNode.addChild(new Position<String>(postOrderMasterList.get(locatedAt), parentNode));
					indexes.add(locatedAt);
				}
			}
			prestart++;
		}
		return parentNode;
	}
	
	public static class Tree<T> {
		private Position<T> root = null;
		
		public Tree() {}
		
		// TODO: Should throw error
		public Position<T> root() {
			if (this.root == null) {
				return null; // Replace with THROWS
			} else {
				return this.root;
			}
		}
		
		public Position<T> parent(Position<T> leaf) {
			if (leaf.parent == null) { // Then its the root
				return null;
			}
			return leaf.parent;
		}
		
		public List<Position<T>> children(Position<T> leaf) {
			return leaf.children();
		}
		
		public void setRoot(Position<T> root) {
			this.root = root;
		}
		
		public void printTree(Position<T> p) {
			System.out.println(p.toString());
			for (int i = 0; i < p.children().size(); i++) {
				printTree(p.children().get(i));
			}
		}
		
		public void levelOrderTraversal(List<Position<String>> level) {
			List<Position<String>> nextLevel = new ArrayList<Position<String>>();
			String levelData = "";
			
			for (int i = 0; i < level.size(); i++) { // iterate positions on level
				if (i != 0) {
					levelData += ", ";
				}
				levelData += level.get(i).getData();
				for (int j = 0; j < level.get(i).children().size(); j++) { // grab children of each position on level
					nextLevel.add(level.get(i).children().get(j));
				}
			}
			System.out.println(levelData);
			if (!nextLevel.isEmpty())
				levelOrderTraversal(nextLevel);
			
		}
	}

	
	public static class Position<T> {
		private T data;
		private Position<T> parent = null;
		private List<Position<T>> children;
		
		public Position(T data, Position<T> parent) {
			this.data = data;
			this.parent = parent;
			this.children = new ArrayList<Position<T>>();
		}
		
		public void addChild(Position<T> child) {
			this.children.add(child);
		}
		
		public void addChildAtIndex(Position<T> child, int index) {
			this.children.add(index, child);
		}
		
		public void setParent(Position<T> parent) {
			this.parent = parent;
		}
		
		public Position<T> getParent() {
			return this.parent;
		}
		
		public List<Position<T>> children() {
			return this.children;
		}
		
		public T getData() {
			return this.data;
		}
		
		public void setData(T data) {
			this.data = data;
		}
		
		public T element() {
			return this.data;
		}
		
		public String toString() {
			if (this.parent == null) {
				return "Data: " + this.data + " Parent: ROOT ELEMENT, Has No Parent";
			} else {
				return "Data: " + this.data + " Parent: " + this.parent.getData();
			}
			
		}
		
	}
}